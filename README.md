# ManPage sobre esperanto
## Estus trankvila kay lernu Esperanton
Un programa para aprender Esperando. La lengua planificada internacional más difundida y hablada en el mundo. Se calcula que es utilizada por 2 millones de personas en más de 120 paises. 

para hacer ejecutable el archivo escribimos
```bash
chmod u+x esperanto.sh
```

para ejecutarlo 

```bash
./esperanto.sh
```
### Cordenada y fuentess de interés para esperantear a fondo:

* El [curso Learn Esperanto](https://learn.esperanto.com/) para empezar, que está muuy bien.
* La [Revista Kontakto](http://kontakto.tejo.org/) muy piola para leer el mundo.
* Palabras relacionadas con [informática](https://komputeko.net/index_es.php).
* La [Wikipedia](https://eo.wikipedia.org/wiki/Vikipedio:%C4%88efpa%C4%9Do) version esperantista.
* El podscat, o mejor dicho: [Podkast](https://kern.punkto.info/). No puedo faltar.
* Y sin un [youtuber ATR](https://www.youtube.com/channel/UCLgkMgy1i9QaOzC_PM9AoRg) no va.
* Una [lista de música](https://www.youtube.com/watch?v=gWiH8BlpU0U&list=PLLg4HNcQo8zx3IMEXcrnRCkEhyXWDDf37&index=2&t=0s) para kajetearla re piola
Dankon pro via atento. Saluton gato!
